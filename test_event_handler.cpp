#include <catch.hpp>
#include <event/event_handler.hh>
#include "test_utils.hh"

SCENARIO("Event handlers can be created from lambda expressions", "[event_handler][lambda_expression]")
{
    GIVEN("An event handler with no template arguments created using lambda expression")
    {
        using event_handler_type = event_handler<>;
        unsigned int counter = 0;
        event_handler_type handler([&counter]() { counter++; });

        THEN("The event handler can be invoked with no arguments")
        {
            REQUIRE(counter == 0);
            handler();
            REQUIRE(counter == 1);
            handler();
            REQUIRE(counter == 2);
        }
    }
    GIVEN("An event handler with one template argument created using lambda expression")
    {
        using event_handler_type = event_handler<int>;
        unsigned int counter = 0;
        event_handler_type handler([&counter](int i) { counter += i; });

        THEN("The event handler can be invoked with one argument")
        {
            REQUIRE(counter == 0);
            handler(5);
            REQUIRE(counter == 5);
            handler(10);
            REQUIRE(counter == 15);
        }
    }
    GIVEN("An event handler with two template arguments created using lambda expression")
    {
        using event_handler_type = event_handler<int, float>;
        float counter = 0;
        event_handler_type handler([&counter](int i, float f) { counter += i + f; });

        THEN("The event handler can be invoked with two arguments")
        {
            REQUIRE(counter == 0.0f);
            handler(5, 5.5f);
            REQUIRE(counter == 10.5f);
            handler(10, 10.5f);
            REQUIRE(counter == 31.0f);
        }
    }
}

SCENARIO("Event handlers can be created from function pointers", "[event_handler][function_pointer]")
{
    GIVEN("An event handler with no template arguments created using function pointer")
    {
        using event_handler_type = event_handler<>;
        event_handler_type handler(&event_handler_no_arguments);

        THEN("The event handler can be invoked with no arguments")
        {
            REQUIRE(counter_no_arguments == 0);
            handler();
            REQUIRE(counter_no_arguments == 1);
            handler();
            REQUIRE(counter_no_arguments == 2);
        }
    }
    GIVEN("An event handler with one template argument created using function pointer")
    {
        using event_handler_type = event_handler<int>;
        unsigned int counter = 0;
        event_handler_type handler(&event_handler_one_argument);

        THEN("The event handler can be invoked with one argument")
        {
            REQUIRE(counter_one_argument == 0);
            handler(5);
            REQUIRE(counter_one_argument == 5);
            handler(10);
            REQUIRE(counter_one_argument == 15);
        }
    }
    GIVEN("An event handler with two template arguments created using function pointer")
    {
        using event_handler_type = event_handler<int, float>;
        event_handler_type handler(&event_handler_two_arguments);

        THEN("The event handler can be invoked with two arguments")
        {
            REQUIRE(counter_two_arguments == 0.0f);
            handler(5, 5.5f);
            REQUIRE(counter_two_arguments == 10.5f);
            handler(10, 10.5f);
            REQUIRE(counter_two_arguments == 31.0f);
        }
    }
}

SCENARIO("Event handlers can be created from functor objects", "[event_handler][functor_object]")
{
    GIVEN("An event handler with no template arguments created using functor object")
    {
        using event_handler_type = event_handler<>;
        functor_no_arguments functor;
        event_handler_type handler = event_handler_type::from_functor(functor);

        THEN("The event handler can be invoked with no arguments")
        {
            REQUIRE(functor.counter == 0);
            handler();
            REQUIRE(functor.counter == 1);
            handler();
            REQUIRE(functor.counter == 2);
        }
    }
    GIVEN("An event handler with one template argument created using functor object")
    {
        using event_handler_type = event_handler<int>;
        functor_one_argument functor;
        event_handler_type handler = event_handler_type::from_functor(functor);

        THEN("The event handler can be invoked with one argument")
        {
            REQUIRE(functor.counter == 0);
            handler(5);
            REQUIRE(functor.counter == 5);
            handler(10);
            REQUIRE(functor.counter == 15);
        }
    }
    GIVEN("An event handler with two template arguments created using functor object")
    {
        using event_handler_type = event_handler<int, float>;
        functor_two_arguments functor;
        event_handler_type handler = event_handler_type::from_functor(functor);

        THEN("The event handler can be invoked with two arguments")
        {
            REQUIRE(functor.counter == 0.0f);
            handler(5, 5.5f);
            REQUIRE(functor.counter == 10.5f);
            handler(10, 10.5f);
            REQUIRE(functor.counter == 31.0f);
        }
    }
}

SCENARIO("Event handlers can be created from class methods", "[event_handler][class_method]")
{
    GIVEN("An event handler with no template arguments created using class method")
    {
        using event_handler_type = event_handler<>;
        callback<int> callback_no_arguments;
        event_handler_type handler = event_handler_type::from_method(callback_no_arguments,
                                                                     &callback<int>::callback_no_arguments);

        THEN("The event handler can be invoked with no arguments")
        {
            REQUIRE(callback_no_arguments.counter == 0);
            handler();
            REQUIRE(callback_no_arguments.counter == 1);
            handler();
            REQUIRE(callback_no_arguments.counter == 2);
        }
    }
    GIVEN("An event handler with one template argument created using class method")
    {
        using event_handler_type = event_handler<int>;
        callback<int> callback_one_argument;
        event_handler_type handler = event_handler_type::from_method(callback_one_argument,
                                                                     &callback<int>::callback_one_argument);

        THEN("The event handler can be invoked with one argument")
        {
            REQUIRE(callback_one_argument.counter == 0);
            handler(5);
            REQUIRE(callback_one_argument.counter == 5);
            handler(10);
            REQUIRE(callback_one_argument.counter == 15);
        }
    }
    GIVEN("An event handler with two template arguments created using class method")
    {
        using event_handler_type = event_handler<int, float>;
        callback<float> callback_two_arguments;
        event_handler_type handler = event_handler_type::from_method(&callback_two_arguments,
                                                                     &callback<float>::callback_two_arguments);

        THEN("The event handler can be invoked with two arguments")
        {
            REQUIRE(callback_two_arguments.counter == 0.0f);
            handler(5, 5.5f);
            REQUIRE(callback_two_arguments.counter == 10.5f);
            handler(10, 10.5f);
            REQUIRE(callback_two_arguments.counter == 31.0f);
        }
    }
}

SCENARIO("Different event handlers will not be evaluated equal", "[event_handler][equality]")
{
    GIVEN("An event_handler is created with no template arguments")
    {
        using event_handler_type = event_handler<>;
        event_handler_type handler1([](){});

        AND_THEN("An other event_handler is created (with no template arguments)")
        {
            event_handler_type handler2([](){});

            THEN("The handlers will not be equal")
            {
                REQUIRE_FALSE(handler1 == handler2);
            }
        }
    }
    GIVEN("An event_handler is created with one template argument")
    {
        using event_handler_type = event_handler<int>;
        event_handler_type handler1([](int){});

        AND_THEN("An other event_handler is created (with one template argument)")
        {
            event_handler_type handler2([](int){});

            THEN("The handlers will not be equal")
            {
                REQUIRE_FALSE(handler1 == handler2);
            }
        }
    }
    GIVEN("An event_handler is created with two template arguments")
    {
        using event_handler_type = event_handler<int, float>;
        event_handler_type handler1([](int, float){});

        AND_THEN("An other event_handler is created (with two template arguments)")
        {
            event_handler_type handler2([](int, float){});

            THEN("The handlers will not be equal")
            {
                REQUIRE_FALSE(handler1 == handler2);
            }
        }
    }
}

SCENARIO("The same event handlers will be evaluated equal", "[event_handler][equality]")
{
    GIVEN("An event_handler is created with no template arguments")
    {
        using event_handler_type = event_handler<>;
        event_handler_type handler1([](){});

        AND_THEN("An other event_handler is created from it")
        {
            event_handler_type handler2 = handler1;

            THEN("The handlers will be equal")
            {
                REQUIRE(handler1 == handler2);
            }
        }
    }
    GIVEN("An event_handler is created with one template argument")
    {
        using event_handler_type = event_handler<int>;
        event_handler_type handler1([](int){});

        AND_THEN("An other event_handler is created from it")
        {
            event_handler_type handler2 = handler1;

            THEN("The handlers will be equal")
            {
                REQUIRE(handler1 == handler2);
            }
        }
    }
    GIVEN("An event_handler is created with two template arguments")
    {
        using event_handler_type = event_handler<int, float>;
        event_handler_type handler1([](int, float){});

        AND_THEN("An other event_handler is created from it")
        {
            event_handler_type handler2 = handler1;

            THEN("The handlers will be equal")
            {
                REQUIRE(handler1 == handler2);
            }
        }
    }
}
