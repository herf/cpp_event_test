#ifndef TEST_UTILS_HH_
#define TEST_UTILS_HH_

extern unsigned int counter_no_arguments;
extern unsigned int counter_one_argument;
extern float counter_two_arguments;

void event_handler_no_arguments();

void event_handler_one_argument(int i);

void event_handler_two_arguments(int i, float f);

struct functor_no_arguments
{
    int counter = 0;

    void operator()();
};

struct functor_one_argument
{
    int counter = 0;

    void operator()(int i);
};

struct functor_two_arguments
{
    float counter = 0.0f;

    void operator()(int i, float f);
};

template<typename counter_type>
class callback
{
  public:
    counter_type counter = 0;

    void callback_no_arguments()
    {
        counter++;
    }

    void callback_one_argument(int i)
    {
        counter += i;
    }

    void callback_two_arguments(int i, float f)
    {
        counter += i + f;
    }
};

#endif
