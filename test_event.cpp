#include <catch.hpp>
#include <event/event.hh>
#include "test_utils.hh"

SCENARIO("Event handlers can be registered to events using the operator +=", "[event][registration]")
{
    GIVEN("An event is created with no template arguments")
    {
        using event_type = event<>;
        event_type event;

        THEN("Lambda expressions can be registered as handlers using the operator +=")
        {
            int counter = 0;

            event += [&counter](){ counter ++; };
            event += [&counter](){ counter += 2; };
            REQUIRE(counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event();
                REQUIRE(counter == 3);
                event();
                REQUIRE(counter == 6);
                AND_THEN("New handlers can be registered")
                {
                    event += [&counter](){ counter += 3; };
                    REQUIRE(counter == 6);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event();
                        REQUIRE(counter == 12);
                        event();
                        REQUIRE(counter == 18);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with one template argument")
    {
        using event_type = event<int>;
        using functor = functor_one_argument;
        event_type event;

        THEN("Functors can be registered as handlers using the operator +=")
        {
            functor functor1;
            functor functor2;

            event += event_type::handler::from_functor(functor1);
            event += event_type::handler::from_functor(functor2);
            REQUIRE(functor1.counter == 0);
            REQUIRE(functor2.counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1);
                REQUIRE(functor1.counter == 1);
                REQUIRE(functor2.counter == 1);
                event(2);
                REQUIRE(functor1.counter == 3);
                REQUIRE(functor2.counter == 3);
                AND_THEN("New handlers can be registered")
                {
                    functor functor3;

                    event += event_type::handler::from_functor(functor3);
                    REQUIRE(functor1.counter == 3);
                    REQUIRE(functor2.counter == 3);
                    REQUIRE(functor3.counter == 0);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event(3);
                        REQUIRE(functor1.counter == 6);
                        REQUIRE(functor2.counter == 6);
                        REQUIRE(functor3.counter == 3);
                        event(4);
                        REQUIRE(functor1.counter == 10);
                        REQUIRE(functor2.counter == 10);
                        REQUIRE(functor3.counter == 7);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with two template arguments")
    {
        using event_type = event<int, float>;
        event_type event;

        THEN("Functors can be registered as handlers using the operator +=")
        {
            using callback_type = callback<float>;
            callback_type callback1;
            callback_type callback2;

            event += event_type::handler::from_method(callback1, &callback<float>::callback_two_arguments);
            event += event_type::handler::from_method(callback2, &callback<float>::callback_two_arguments);
            REQUIRE(callback1.counter == 0.0f);
            REQUIRE(callback2.counter == 0.0f);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1, 1.0f);
                REQUIRE(callback1.counter == 2.0f);
                REQUIRE(callback2.counter == 2.0f);
                event(2, 2.0f);
                REQUIRE(callback1.counter == 6.0f);
                REQUIRE(callback2.counter == 6.0f);
                AND_THEN("New handlers can be registered")
                {
                    callback_type callback3;

                    event += event_type::handler::from_method(callback3, &callback<float>::callback_two_arguments);
                    REQUIRE(callback1.counter == 6.0f);
                    REQUIRE(callback2.counter == 6.0f);
                    REQUIRE(callback3.counter == 0.0f);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event(3, 3.0f);
                        REQUIRE(callback1.counter == 12.0f);
                        REQUIRE(callback2.counter == 12.0f);
                        REQUIRE(callback3.counter == 6.0f);
                        event(4, 4.0f);
                        REQUIRE(callback1.counter == 20.0f);
                        REQUIRE(callback2.counter == 20.0f);
                        REQUIRE(callback3.counter == 14.0f);
                    }
                }
            }
        }
    }
}

SCENARIO("Event handlers can be registered to events using the subscribe method", "[event][registration]")
{
    GIVEN("An event is created with no template arguments")
    {
        using event_type = event<>;
        event_type event;

        THEN("Lambda expressions can be registered as handlers using the subscribe method")
        {
            int counter = 0;

            event.subscribe([&counter](){ counter ++; });
            event.subscribe([&counter](){ counter += 2; });
            REQUIRE(counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event();
                REQUIRE(counter == 3);
                event();
                REQUIRE(counter == 6);
                AND_THEN("New handlers can be registered")
                {
                    event.subscribe([&counter](){ counter += 3; });
                    REQUIRE(counter == 6);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event();
                        REQUIRE(counter == 12);
                        event();
                        REQUIRE(counter == 18);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with one template argument")
    {
        using event_type = event<int>;
        using functor = functor_one_argument;
        event_type event;

        THEN("Functors can be registered as handlers using the subscribe method")
        {
            functor functor1;
            functor functor2;

            event.subscribe(event_type::handler::from_functor(functor1));
            event.subscribe(event_type::handler::from_functor(functor2));
            REQUIRE(functor1.counter == 0);
            REQUIRE(functor2.counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1);
                REQUIRE(functor1.counter == 1);
                REQUIRE(functor2.counter == 1);
                event(2);
                REQUIRE(functor1.counter == 3);
                REQUIRE(functor2.counter == 3);
                AND_THEN("New handlers can be registered")
                {
                    functor functor3;

                    event.subscribe(event_type::handler::from_functor(functor3));
                    REQUIRE(functor1.counter == 3);
                    REQUIRE(functor2.counter == 3);
                    REQUIRE(functor3.counter == 0);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event(3);
                        REQUIRE(functor1.counter == 6);
                        REQUIRE(functor2.counter == 6);
                        REQUIRE(functor3.counter == 3);
                        event(4);
                        REQUIRE(functor1.counter == 10);
                        REQUIRE(functor2.counter == 10);
                        REQUIRE(functor3.counter == 7);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with two template arguments")
    {
        using event_type = event<int, float>;
        event_type event;

        THEN("Methods can be registered as handlers using the subscribe method")
        {
            using callback_type = callback<float>;
            callback_type callback1;
            callback_type callback2;

            event.subscribe(event_type::handler::from_method(callback1, &callback<float>::callback_two_arguments));
            event.subscribe(event_type::handler::from_method(callback2, &callback<float>::callback_two_arguments));
            REQUIRE(callback1.counter == 0.0f);
            REQUIRE(callback2.counter == 0.0f);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1, 1.0f);
                REQUIRE(callback1.counter == 2.0f);
                REQUIRE(callback2.counter == 2.0f);
                event(2, 2.0f);
                REQUIRE(callback1.counter == 6.0f);
                REQUIRE(callback2.counter == 6.0f);
                AND_THEN("New handlers can be registered")
                {
                    callback_type callback3;

                    event.subscribe(event_type::handler::from_method(callback3,
                                                                     &callback<float>::callback_two_arguments));
                    REQUIRE(callback1.counter == 6.0f);
                    REQUIRE(callback2.counter == 6.0f);
                    REQUIRE(callback3.counter == 0.0f);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event(3, 3.0f);
                        REQUIRE(callback1.counter == 12.0f);
                        REQUIRE(callback2.counter == 12.0f);
                        REQUIRE(callback3.counter == 6.0f);
                        event(4, 4.0f);
                        REQUIRE(callback1.counter == 20.0f);
                        REQUIRE(callback2.counter == 20.0f);
                        REQUIRE(callback3.counter == 14.0f);
                    }
                }
            }
        }
    }
}

SCENARIO("Event handlers can be registered to and de-registered from events using the operator+=/operator-=",
         "[event][registration]")
{
    GIVEN("An event is created with no template arguments")
    {
        using event_type = event<>;
        event_type event;

        THEN("Lambda expressions can be registered as handlers using the operator+=")
        {
            int counter = 0;
            event_type::handler handler1 = [&counter](){ counter ++; };
            event_type::handler handler2 = [&counter](){ counter += 2; };

            event += handler1;
            event += handler2;
            REQUIRE(counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event();
                REQUIRE(counter == 3);
                event();
                REQUIRE(counter == 6);
                AND_THEN("Event handlers can be de-registered using the operator-= method")
                {
                    event -= handler2;
                    REQUIRE(counter == 6);
                    AND_THEN("Firing the event will call the remaining registered handlers")
                    {
                        event();
                        REQUIRE(counter == 7);
                        event();
                        REQUIRE(counter == 8);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with one template argument")
    {
        using event_type = event<int>;
        using functor = functor_one_argument;
        event_type event;

        THEN("Functors can be registered as handlers using the operator+=")
        {
            functor functor1;
            functor functor2;
            event_type::handler handler1 = event_type::handler::from_functor(functor1);
            event_type::handler handler2 = event_type::handler::from_functor(functor2);

            event += handler1;
            event += handler2;
            REQUIRE(functor1.counter == 0);
            REQUIRE(functor2.counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1);
                REQUIRE(functor1.counter == 1);
                REQUIRE(functor2.counter == 1);
                event(2);
                REQUIRE(functor1.counter == 3);
                REQUIRE(functor2.counter == 3);
                AND_THEN("Event handlers can be de-registered using the operator-=")
                {
                    event -= handler2;
                    REQUIRE(functor1.counter == 3);
                    REQUIRE(functor2.counter == 3);
                    AND_THEN("Firing the event will call the remaining registered handlers")
                    {
                        event(3);
                        REQUIRE(functor1.counter == 6);
                        REQUIRE(functor2.counter == 3);
                        event(4);
                        REQUIRE(functor1.counter == 10);
                        REQUIRE(functor2.counter == 3);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with two template arguments")
    {
        using event_type = event<int, float>;
        event_type event;

        THEN("Methods can be registered as handlers using the operator+=")
        {
            using callback_type = callback<float>;
            callback_type callback1;
            callback_type callback2;
            event_type::handler handler1 = event_type::handler::from_method(callback1,
                                                                            &callback<float>::callback_two_arguments);
            event_type::handler handler2 = event_type::handler::from_method(callback2,
                                                                            &callback<float>::callback_two_arguments);

            event += handler1;
            event += handler2;
            REQUIRE(callback1.counter == 0.0f);
            REQUIRE(callback2.counter == 0.0f);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1, 1.0f);
                REQUIRE(callback1.counter == 2.0f);
                REQUIRE(callback2.counter == 2.0f);
                event(2, 2.0f);
                REQUIRE(callback1.counter == 6.0f);
                REQUIRE(callback2.counter == 6.0f);
                AND_THEN("Event handlers can be de-registered using the operator-=")
                {
                    event -= handler2;
                    REQUIRE(callback1.counter == 6.0f);
                    REQUIRE(callback2.counter == 6.0f);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event(3, 3.0f);
                        REQUIRE(callback1.counter == 12.0f);
                        REQUIRE(callback2.counter == 6.0f);
                        event(4, 4.0f);
                        REQUIRE(callback1.counter == 20.0f);
                        REQUIRE(callback2.counter == 6.0f);
                    }
                }
            }
        }
    }
}

SCENARIO("Event handlers can be registered to and de-registered from events using the subscribe/unsubscribe methods",
         "[event][registration]")
{
    GIVEN("An event is created with no template arguments")
    {
        using event_type = event<>;
        event_type event;

        THEN("Lambda expressions can be registered as handlers using the subscribe method")
        {
            int counter = 0;
            event_type::handler handler1 = [&counter](){ counter ++; };
            event_type::handler handler2 = [&counter](){ counter += 2; };

            event.subscribe(handler1);
            event.subscribe(handler2);
            REQUIRE(counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event();
                REQUIRE(counter == 3);
                event();
                REQUIRE(counter == 6);
                AND_THEN("Event handlers can be de-registered using the un-subscribe method")
                {
                    event.unsubscribe(handler2);
                    REQUIRE(counter == 6);
                    AND_THEN("Firing the event will call the remaining registered handlers")
                    {
                        event();
                        REQUIRE(counter == 7);
                        event();
                        REQUIRE(counter == 8);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with one template argument")
    {
        using event_type = event<int>;
        using functor = functor_one_argument;
        event_type event;

        THEN("Functors can be registered as handlers using the subscribe method")
        {
            functor functor1;
            functor functor2;
            event_type::handler handler1 = event_type::handler::from_functor(functor1);
            event_type::handler handler2 = event_type::handler::from_functor(functor2);

            event.subscribe(handler1);
            event.subscribe(handler2);
            REQUIRE(functor1.counter == 0);
            REQUIRE(functor2.counter == 0);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1);
                REQUIRE(functor1.counter == 1);
                REQUIRE(functor2.counter == 1);
                event(2);
                REQUIRE(functor1.counter == 3);
                REQUIRE(functor2.counter == 3);
                AND_THEN("Event handlers can be de-registered using the un-subscribe method")
                {
                    event.unsubscribe(handler2);
                    REQUIRE(functor1.counter == 3);
                    REQUIRE(functor2.counter == 3);
                    AND_THEN("Firing the event will call the remaining registered handlers")
                    {
                        event(3);
                        REQUIRE(functor1.counter == 6);
                        REQUIRE(functor2.counter == 3);
                        event(4);
                        REQUIRE(functor1.counter == 10);
                        REQUIRE(functor2.counter == 3);
                    }
                }
            }
        }
    }
    GIVEN("An event is created with two template arguments")
    {
        using event_type = event<int, float>;
        event_type event;

        THEN("Methods can be registered as handlers using the subscribe method")
        {
            using callback_type = callback<float>;
            callback_type callback1;
            callback_type callback2;
            event_type::handler handler1 = event_type::handler::from_method(callback1,
                                                                            &callback<float>::callback_two_arguments);
            event_type::handler handler2 = event_type::handler::from_method(callback2,
                                                                            &callback<float>::callback_two_arguments);

            event.subscribe(handler1);
            event.subscribe(handler2);
            REQUIRE(callback1.counter == 0.0f);
            REQUIRE(callback2.counter == 0.0f);
            AND_THEN("Firing the event will call the registered handlers")
            {
                event(1, 1.0f);
                REQUIRE(callback1.counter == 2.0f);
                REQUIRE(callback2.counter == 2.0f);
                event(2, 2.0f);
                REQUIRE(callback1.counter == 6.0f);
                REQUIRE(callback2.counter == 6.0f);
                AND_THEN("Event handlers can be de-registered using the un-subscribe method")
                {
                    event.unsubscribe(handler2);
                    REQUIRE(callback1.counter == 6.0f);
                    REQUIRE(callback2.counter == 6.0f);
                    AND_THEN("Firing the event will call all the registered handlers")
                    {
                        event(3, 3.0f);
                        REQUIRE(callback1.counter == 12.0f);
                        REQUIRE(callback2.counter == 6.0f);
                        event(4, 4.0f);
                        REQUIRE(callback1.counter == 20.0f);
                        REQUIRE(callback2.counter == 6.0f);
                    }
                }
            }
        }
    }
}

class test_deregister
{
  public:
    using event_type = event<>;

    int counter = 0;
    event_type event_;
    event_type::handler event_handler_a;
    event_type::handler event_handler_b;

    test_deregister():
       event_handler_a(event_type::handler::from_method(this, &test_deregister::event_handler1)),
       event_handler_b(event_type::handler::from_method(this, &test_deregister::event_handler2))
    {
        event_ += event_handler_a;
        event_.subscribe(event_handler_b);
    }

    void event_handler1()
    {
        counter += 1;
        if(counter > 3)
        {
            event_ -= event_handler_b;
        }
        if(counter > 6)
        {
            event_.unsubscribe(event_handler_a);
        }
    }

    void event_handler2()
    {
        counter += 2;

    }
};

TEST_CASE("Event handlers can de-register themselves during event execution", "[event][de-registration]")
{
    test_deregister deregister;

    REQUIRE(deregister.counter == 0);
    // Both listener should be fired
    deregister.event_();
    REQUIRE(deregister.counter == 3);
    // Both listener should be fired, event_handler2 should be de-registered
    deregister.event_();
    REQUIRE(deregister.counter == 6);
    // Only event_handler1 should be fired, and then de-registered
    deregister.event_();
    REQUIRE(deregister.counter == 7);
    // No event handlers should be registered by now
    deregister.event_();
    REQUIRE(deregister.counter == 7);
}
