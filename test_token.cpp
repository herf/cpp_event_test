#include <catch.hpp>
#include <event/token.hh>

SCENARIO("Different tokens will not be evaluated equal", "[token][equality]")
{
    GIVEN("A token is created")
    {
        token t1;

        AND_THEN("An other token is created")
        {
            token t2;

            THEN("The tokens will not be equal")
            {
                REQUIRE_FALSE(t1 == t2);
            }
        }
    }
}

SCENARIO("The same tokens will be evaluated equal", "[token][equality]")
{
    GIVEN("A token is created")
    {
        token t1;

        AND_THEN("An other token is created from it")
        {
            token t2 = t1;

            THEN("The tokens will be equal")
            {
                REQUIRE(t1 == t2);
            }
        }
    }
}


#include <thread>
#include <unordered_set>
#include <mutex>

SCENARIO("Tokens created in different threads might have the same id if the code is built without threading support,"
         " but with threading support this should not happen",
         "[token][threading]")
{
    std::unordered_set<token> token_set;
    std::mutex set_mutex;
    std::thread thread_1([&token_set, &set_mutex]()
    {
        for(unsigned int i = 0; i < 100000; ++i)
        {
            token t;
            std::lock_guard<std::mutex> l(set_mutex);

            token_set.insert(t);
        }
    });
    std::thread thread_2([&token_set, &set_mutex]()
    {
        for(unsigned int i = 0; i < 100000; ++i)
        {
            token t;
            std::lock_guard<std::mutex> l(set_mutex);

            token_set.insert(t);
        }
    });

    thread_1.join();
    thread_2.join();
#ifndef __EVENT_USE_THREADS__
    // If the code was built without threading, we cannot really test here anything.
    // Sometimes everything will be OK, sometimes there will be tokens with the same ID
    if(token_set.size() != 200000)
    {
        WARN("There were tokens with the same id. It is normal if threads are not supported");
    }
    else
    {
        WARN("All tokens had unique id. This also can happen even without thread support.");
    }
#else
    // If the code was built with threading, we should always have tokens with different IDs,
    // so inserting them into the set, should always create a new item,
    // so the number of tokens in the set should be 200.000
    REQUIRE(token_set.size() == 200000);
#endif
}



