#include "test_utils.hh"

unsigned int counter_no_arguments = 0;
unsigned int counter_one_argument = 0;
float counter_two_arguments = 0.0f;

void event_handler_no_arguments()
{
    counter_no_arguments++;
}

void event_handler_one_argument(int i)
{
    counter_one_argument += i;
}

void event_handler_two_arguments(int i, float f)
{
    counter_two_arguments += i + f;
}


void functor_no_arguments::operator()()
{
    counter++;
}

void functor_one_argument::operator()(int i)
{
    counter += i;
}

void functor_two_arguments::operator()(int i, float f)
{
    counter += i + f;
}
